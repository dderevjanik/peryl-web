# Peryl

Q: View bez state, teda nemusim pisat explicitne View<any> ?
- niekedy to je dobre, lebo pri default mozem zabudnut (automaticky pretypuje na `any`) a potom sa mi objavi chyba v runtime
Q: Nefunguje s Hsml :(, musim pouzit Hsmls
- mozno uvazovat nad `Hsml` a `Fragment`
- je potrebne to zanechat. Aj `React` robi akoby `Fragment`
- mozeme to prenechat aj na factory, ale potom musim detekovat ci sa jedna o `Hsml` alebo `Hsmls`, lebo ak `Hsml`, tak treba pridat `[]`

Q: bez View.type-u to nejde, to mozem vysvetlit
- je potrebne to zanechat aby v runtime bolo mozne namountovat `View.type`
Q: bez View.state-u to nejde aj napriek View<any>
- tu by mohol pomoct default state `{}`

Q: Je divne (aspon na prvy pohlad), ze HelloWorld je View a my z neho vyrabame `new Ctrl`. Mozno zmenit naming ?
- ?
Q: Ctrl celym nazvom Controler ?
- ?

Q: CDN ? Pre live examples
- z
Q: Lepsie imports (aby to bolo priamo z improt { View, Ctrl } "peryl")
- z
Q: `.actions`
- z
