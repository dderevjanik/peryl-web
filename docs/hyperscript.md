---
id: hyperscript
title: Hypescript
---

Writing [hsml](hsml.md) could be sometimes frustrating, even typescript can't
fully understand recursive hsml syntax and throws vague errors. We preperad hypertext
helpers, which helps you to write non-breaking `hsml` structure using functions.

Hyperscript is based on template library [https://github.com/hyperhype/hyperscript](https://github.com/hyperhype/hyperscript) and its
intent is to provide function wrapper above HSML syntax to make it more typescript friendly.

```typescript
import { h } from "peryl/dist/hsml-h";
h("div");
// is similar to: ["div"]

h("span#app.body");
// is similar to: ["span#app.body"]

h("div", { click: "special" });
// is similar to: ["div", { click: "special" }]);

h("ul", [
    h("li", "1. line"),
    h("li", "2. line"),
    h("li", "3. line")
]);
// ["ul", [
//     ["li", ["1. line"]],
//     ["li", ["2. line"]],
//     ["li", ["3. line"]]
// ]]
```

Still, we recommend using `hsml` syntax over `hsml-h` helpers.
