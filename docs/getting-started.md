---
id: getting-started
title: Getting Started
---

The official guide assumes basic level knowledge of HTML, CSS, and JavaScript. If you are totally new to development, it might not be the best idea to jump right into a framework as your first step - grasp the basics then come back Prior experience with other frameworks helps, but is not required.

## 1. Rendering a View

At the core of Peryl is a system that enables us to declaratively render data to the DOM using straightforward [Hsml](hsml.md) syntax:

<!-- <a class="jsbin-embed" href="https://jsbin.com/mizinog/embed?html,output">JS Bin on jsbin.com</a><script src="https://static.jsbin.com/js/embed.min.js?4.1.7"></script> -->
<!--
<iframe width="100%" height="400" src="https://jsitor.com/embed/pP8Ea3aJR?html&result&" frameborder="0"></iframe>

<iframe witdh="100%" height="400" src="http://embed.plnkr.co/zI5FeO/" frameborder="1"></iframe> -->
<iframe width="100%" height="400" src="//jsfiddle.net/ateamsolutions/v0ho3pfr/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

We have already created our very first Peryl app! This looks pretty similar to rendering a string template, but Peryl has done a lot of work under the hood. The data and the DOM are now linked.

## 2. Composing Views

Views are javascript functions, we can use them to create reusable views in out application.

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/sbk5y0tp/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

Learn more about Hsml from [Hsml Reference](hsml.md)
