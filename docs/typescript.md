---
id: typescript
title: Using Typescript
sidebar_label: Using Typescript
---

WIP

## 1. Installation

In order to use typescript in our project, we would need to install `peryl` package first. Peryl is written in typescript, so you don't need to install an additional `@types` library.

```sh
npm i peryl
```

## 2. View

```ts
// page.ts
import { View } from "peryl/dist/hsml-app";

export interface PageState {
  txt: string;
}

export const pageState: PageState = {
  txt: ""
};

export enum PageActions {
  CHANGE_TXT = "CHANGE_TXT"
}

export const page: View<PageState> = state => {
  return [
    [
      "div",
      [
        ["h1", ["Hello Examples"]],
        ["input", { value: state.txt, on: ["input", PageActions.CHANGE_TXT] }],
        ["p", ["Hello" + state.txt]]
      ]
    ]
  ];
};
```

## 3. Main App

```ts
// app.ts
import { page, pageState, PageState, PageActions } from "./page";

function actions(app, action, data, ev) {
  if (action === PageActions.CHANGE_TXT) {
    app.txt = ev.target.value;
    app.update();
  }
}

const app = new App<PageState>(pageState, page, actions).mount("app");
```
