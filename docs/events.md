---
id: events
title: Handling Events
---

Peryl's event-based concept is similar to ELM. In peryl, View can dispatch action when an DOM element's event is emitted.
With `"on"` attribute we can attach event listeners to listen on users interactions and dispatch `Action`. Let's see examples below.

![peryl-app-flow](assets/peryl-app-flow.png)

## 1. Alert on user click

In this example we are going to attach `click` listener to button element. Every time user clicks on button `SHOW_ALERT` action will be dispatched.

All dispatched actions are caught by `function actions`, where we check which kind of
action is incoming. If incoming action is `SHOW_ALERT` then we display alert box.

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/yw27bkqp/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

## 2. Change data based on user input

Lets see reactive example with text input.

In previous section [Dealing with state](app-state.md) we have seen that we can mutate app's state
by directly accessing `app.state` and then rerender application using `app.update()`.

Example below is combination of dispatching `Action`, catching it in `function actions`,
mutating app's state and then finally update it using `app.update()`.

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/6y04jbxq/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>

## 3. Making a counter

<iframe width="100%" height="600" src="//jsfiddle.net/ateamsolutions/jt2p7s8u/embedded/html,result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
