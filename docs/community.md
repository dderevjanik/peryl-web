---
id: community
title: Community
sidebar_label: Example Page
---

## Sources

Official website [https://gitlab.com/peter-rybar/peryl](https://gitlab.com/peter-rybar/peryl)

Link to this documentation [https://gitlab.com/dderevjanik/peryl-web](https://gitlab.com/dderevjanik/peryl-web)

## Tools

Online Html to Hsml convertor [https://dderevjanik.github.io/html2hsml/](https://dderevjanik.github.io/html2hsml/)

## References

Hyperscript Tools is based on [https://github.com/hyperhype/hyperscript](https://github.com/hyperhype/hyperscript)

Origin of HSML [http://www.jsonml.org/](http://www.jsonml.org/)
